#define _GNU_SOURCE
#include <stdio.h>
#include <string.h>
#include <getopt.h>
#include <time.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>

#define t 1
#define u 2
#define n 7
#define o 3
/* p = 3
q = 3
v = 2
r = 2

pause = t
packet_count = 0
*/
#define TRUE 1
#define FALSE 0


#define SUBS_REQ 0x00
#define SUBS_ACK 0x01
#define SUBS_REJ 0x02
#define SUBS_INFO 0x03
#define INFO_ACK 0x04
#define SUBS_NACK 0x05
#define NEW_SUB 0x06
#define RANDOM 0x07

#define DISCONNECTED 0xa0
#define NOT_SUBSCRIBED 0xa1
#define WAIT_ACK_SUBS 0xa2
#define WAIT_INFO 0xa3
#define WAIT_ACK_INFO 0xa4
#define SUBSCRIBED 0xa5
#define SEND_HELLO 0xa6

#define HELLO 0x10
#define HELLO_REJ 0x11
#define SEND_DATA 0x20
#define SET_DATA 0x21
#define GET_DATA 0x22
#define DATA_ACK 0x23
#define DATA_NACK 0x24
#define DATA_REJ 0x25

#define LONGDADES 103



/*definim les estructures que utilitzarem*/
struct
{
	char *nom;
	int val;
} state[7] =
{
	{ "DISCONNECTED",0xa0},
	{ "NOT_SUBSCRIBED",0xa1},
	{ "WAIT_ACK_SUBS",0xa2},
	{ "WAIT_INFO",0xa3},
	{ "WAIT_ACK_INFO",0xa4},
	{ "SUBSCRIBED",0xa5},
	{ "SEND_HELLO",0xa6},
};

struct
{
	char *nom;
	int val;
} SecondFaseType[2] =
{
	{ "HELLO",0x10},
	{ "HELLO_REJ",0x11},
};

struct
{
	char *nom;
	int val;
} SubsTypes[7] =
{
	{ "SUBS_REQ",0x00},
	{ "SUBS_ACK",0x01},
	{ "SUBS_REJ",0x02},
	{ "SUBS_INFO",0x03},
	{ "INFO_ACK",0x04},
	{ "SUBS_NACK",0x05},
};

struct dadesFill
{
	int type;
	int id;
	char number[9];
	char situacio[13];
	int estat;
	int tcpPort;
	char actuadors[5][8];
	char elements[41];
	int actNumero;
	char IP [16];

};

struct UdpPDU
{
	unsigned char type;
	char mac[13];
	char number[9];
	char dades[80];
};

struct TcpPDU
{
	unsigned char type;
	char mac[13];
	char number[9];
	char dades[80];
};

struct Controlador
{
    char name[9];
    char mac[13];
		char number[9];
		char situacio[13];
		int estat;
		int tcpPort;
		char actuadors[5][8];
		char elements[41];
		int actNumero;
		char IP [16];
		int lastHello;
		int noHello;
};

struct Server
{
	char name[10];
	char mac[13];
	int udpPort;
	int tcpPort;
};

/*variables globals*/
FILE *configuration_file;
FILE *controladors_file;
int sock, sockUDP,infoSocket;
struct Controlador acceptedControladors[20],newData;
struct Server server;

int nombreControladors;
char documentCFG[20];
char documentControladors[20];
int controlador;
struct sockaddr_in	addr_server,addr_cli,tmp;
struct UdpPDU dadEnv, paquetUDP;
socklen_t sizeAddr;
char mis[500];
struct UdpPDU dadReb;
int debug;
int infoPort;


int pfd[2];



/*definim les funcions*/
/*proces de Subs*/
void sendSubRej();
void subsFase(struct UdpPDU);
void tractarDadesSub(struct UdpPDU);
void generarSubAckPDU();
void enviarACK();
void infoFase(struct UdpPDU);
int generateRandom();
void determinateNewUDP();
void tractarDadesInfo(struct UdpPDU);
void sendAckINFO();
void enviarInfoAck();
void generarInfoAckPDU();

/*utils*/
char* paquetType(int);
void msg(char *);
int determinateControler(char []);
char * getState(int );
int comprovarPaquetUDP(struct UdpPDU);
void dbg(char*);
int getType(int);
/*configuracions*/
void configureControladors();
void configureServer();
void situateStartData(char *data);








int main(int argc, char **argv)
{

	int opt;
	fd_set listFD;
  struct timeval timeOut;
	int sizePDU;
	struct dadesFill info;
	int fd;
	int x;
	time_t rawtime;
	struct tm * timeinfo;


	srand(time(NULL));
    debug = FALSE;
	strcpy(documentCFG,"./server.cfg");
    strcpy(documentControladors,"./controlers.dat");

	while ((opt = getopt(argc, argv,"dc:u:")) != -1) {
		switch (opt){
	            case 'c':
								strcpy(documentCFG,optarg);
                                break;
                case 'u':
                                strcpy(documentControladors,optarg);
                                break;
                case 'd':
                                debug = TRUE;

                    }

    	}



	/* configuracions */
	configureServer();
	configureControladors();

	/* piper per pasar la info dels fills al pare*/
	pipe(pfd);
	dup2(pfd[0],3);
  dup2(pfd[1],4);

	/*open UDPport*/
	sock=socket(AF_INET,SOCK_DGRAM,0);
	if(sock<0)
	{
		fprintf(stderr,"No puc obrir socket!!!\n");
		perror(argv[0]);
		exit(-1);
	}

	/*
	Bind del socket, procencia qualsevol IP
	*/
	memset(&addr_server,0,sizeof (struct sockaddr_in));
	addr_server.sin_family=AF_INET;
	addr_server.sin_addr.s_addr=htonl(INADDR_ANY);
	addr_server.sin_port=htons(server.udpPort);
	if(bind(sock,(struct sockaddr *)&addr_server,sizeof(struct sockaddr_in))<0)
	{
		fprintf(stderr,"Error al fer el binding del socket\n");
                exit(1);
	}




	sizePDU = sizeof(struct UdpPDU);

	sizeAddr = sizeof(addr_cli);

	while(1)
	{
		FD_ZERO(&listFD);
		FD_SET(0, &listFD);
		FD_SET(3, &listFD);
		FD_SET(sock, &listFD);


		timeOut.tv_sec = 2;
		timeOut.tv_usec = 0;
		fd = select(sock+1, &listFD, NULL, NULL, &timeOut);
		if (fd){
		if(FD_ISSET(3,&listFD)){

			struct dadesFill novesDades;


			memset(&novesDades,0,sizeof(struct dadesFill));
			read(3,&novesDades,sizeof(struct dadesFill));
			switch(novesDades.type){
				case (NEW_SUB):



				time ( &rawtime );
				timeinfo = localtime ( &rawtime );

					strcpy(acceptedControladors[novesDades.id].number,novesDades.number);
					acceptedControladors[novesDades.id].estat = novesDades.estat;

					break;

				case(RANDOM):
					memset(&info,0,sizeof(struct dadesFill));

					strcpy(info.number,acceptedControladors[controlador].number);
					break;
				case(HELLO):
					acceptedControladors[novesDades.id].estat = novesDades.estat;
					acceptedControladors[novesDades.id].lastHello = novesDades.actNumero;
					break;

			}
		}

		if(FD_ISSET(sock, &listFD)){




											if (fork() == 0) /*creem el fill per atendre el client */
											{

												if (recvfrom(sock,&dadReb,sizePDU,0,(struct sockaddr *)&addr_cli,&sizeAddr)>0)
												{

													sprintf(mis,"Rebut bytes=%lu, comanda=%s, mac=%s, rndm=%s, dades=%s,",sizeof(dadReb),paquetType(dadReb.type),dadReb.mac,dadReb.number,dadReb.dades);
													dbg(mis);

													switch(dadReb.type)
													{



													 case(SUBS_REQ):
	                        /* temporitzadors i comprovacions*/
	                        if((controlador = determinateControler(dadReb.mac)) < 0 ){
	                            sendSubRej();
															exit(0);
	                        }

	                        if(acceptedControladors[controlador].estat == DISCONNECTED)
	                        {
	                            /*montem el nou socket i port*/


															sockUDP = socket(AF_INET,SOCK_DGRAM,0);
																														/*  */
	                            subsFase(dadReb);

															setsockopt(sockUDP,SOL_SOCKET,SO_RCVTIMEO,&timeOut,sizeof(timeOut));

	                            if(acceptedControladors[controlador].estat == WAIT_INFO){

	                              if (recvfrom(sockUDP,&dadReb,sizeof(struct UdpPDU),0,(struct sockaddr *)&tmp,&sizeAddr)>0){
																	int a;


																	sprintf (mis,"Rebut bytes=%lu, comanda=%s, mac=%s, rndm=%s, dades=%s,",sizeof(dadReb),paquetType(dadReb.type),dadReb.mac,dadReb.number,dadReb.dades);
	                              	dbg(mis);
																	infoFase(dadReb);


																	a = sendto(sockUDP,&dadEnv,sizeof(dadEnv),0,(struct sockaddr*)&tmp,sizeAddr);
																	if(a > 0)
																	{
																		sprintf(mis,"Enviat:  bytes=%i, comanda=%s, mac=%s, rndm=%s, dades=%s",a,paquetType(dadEnv.type),dadEnv.mac,dadEnv.number,dadEnv.dades);
																		dbg(mis);

																	}else{
																		sprintf(mis,"ERROR %i al enviar el paquet: %s",a,paquetType(dadEnv.type));
																		msg(mis);
																	}
																}
															}

															if(acceptedControladors[controlador].estat == SUBSCRIBED)
															{
																int a;

																/*actualitzarDadesPare();*/



																memset(&info,0,sizeof(struct dadesFill));

																info.type = NEW_SUB;
																info.id = controlador;

																strcpy(info.situacio,acceptedControladors[controlador].situacio);
																info.estat = acceptedControladors[controlador].estat;
																info.tcpPort = acceptedControladors[controlador].tcpPort;
																for(a = 0; a<5; a++)
																{
																	strcpy(info.actuadors[a],acceptedControladors[controlador].actuadors[a]);
																}
																strcpy(info.elements,acceptedControladors[controlador].elements);
																	strcpy(info.number,acceptedControladors[controlador].number);
																strcpy(info.IP,acceptedControladors[controlador].IP);
																info.actNumero = acceptedControladors[controlador].actNumero;

																write(4,&info,sizeof(struct dadesFill));

																exit(1);
															}else{

															}

	                            exit(0);

	                        }else{

	                            /*sendSubRej();*/

				                			exit(0);

	                        }

													case(HELLO):
														controlador = determinateControler(dadReb.mac);

													if(comprovarPaquetUDP(dadReb)){
														int a;
														int lastHello;
														struct UdpPDU dadEnv;

														if(acceptedControladors[controlador].estat != SEND_HELLO )
														{
															acceptedControladors[controlador].estat = SEND_HELLO;
															sprintf(mis,"el controlador %s passa a l'estat %s",acceptedControladors[controlador].name,getState(acceptedControladors[controlador].estat));
															msg(mis);
														}



														memset(&dadEnv,0,sizeof(struct UdpPDU));
														dadEnv.type = HELLO;
														strcpy(dadEnv.mac,server.mac);
														strcpy(dadEnv.number,acceptedControladors[controlador].number);
														strcpy(dadEnv.dades,dadReb.dades);
														a = sendto(sock,&dadEnv,sizeof(dadEnv),0,&addr_cli,sizeAddr);
														sprintf(mis,"Enviat:  bytes=%i, comanda=%s, mac=%s, rndm=%s, dades=%s",a,paquetType(dadEnv.type),dadEnv.mac,dadEnv.number,dadEnv.dades);
														dbg(mis);

														time ( &rawtime );
												    timeinfo = localtime ( &rawtime );

														lastHello = timeinfo->tm_sec;
														memset(&info,0,sizeof(struct dadesFill));
														info.type = HELLO;
														info.actNumero = lastHello;
														/*strcpy(info.number,acceptedControladors[controlador].number);*/
														info.estat = acceptedControladors[controlador].estat;
														write(4,&info,sizeof(struct dadesFill));
														exit(0);
														break;
												}
												}
												exit(0);
								}
								printf("adeu %i\n", getpid() );
								exit(0);
				}
		}
		if(FD_ISSET(0, &listFD)){
			if(fork() == 0){
				char buff[20];
				char temp[20];
				char list[] = {"list"};
				int x,y;

				fgets(buff, sizeof(buff), stdin);

				x = 0;

				while(buff[x] != '\n' && buff[x] != ' ' && buff[x] != '\0')
				{
					temp[x] = buff[x];
					x++;
				}
				temp[x] = '\0';
				if((strcmp(temp,list) == 0))
				{
					printf("--NOM--- ------IP------- -----MAC---- --RNDM-- ----ESTAT--- --SITUACIÓ-- --ELEMENTS-------------------------------------------\n");
					for(y = 0; y < nombreControladors;y++)
					{
						printf("%s %s %s %s %s %s %s \n",acceptedControladors[y].name,acceptedControladors[y].IP,acceptedControladors[y].mac,acceptedControladors[y].number,getState(acceptedControladors[y].estat), acceptedControladors[y].situacio,acceptedControladors[y].elements) ;
					}
				}
				if((strcmp(temp,"quit") == 0)){
					kill(getppid(), SIGKILL);
				}
				exit(0);
			}
		}
	}



	for( x = 0; x< nombreControladors;x++){
		if(acceptedControladors[x].estat == SEND_HELLO)
		{
			time ( &rawtime );
	    timeinfo = localtime ( &rawtime );

			if( (timeinfo->tm_sec- acceptedControladors[x].lastHello) > 2)
			{
				acceptedControladors[x].noHello ++;
			}
			if(acceptedControladors[x].noHello > 3){
				strcpy(acceptedControladors[x].number,"00000000");
				acceptedControladors[x].estat = DISCONNECTED;
				sprintf(acceptedControladors[x].situacio,"            ");
				sprintf(acceptedControladors[x].IP,"               ");
				sprintf(acceptedControladors[x].elements,"       ");
				acceptedControladors[x].noHello = 0;
				sprintf(mis,"Controlador: %s [%s] no ha rebut 3 Hellos consecutius",acceptedControladors[x].name,acceptedControladors[x].mac);
				msg(mis);
				sprintf(mis,"Controlador: %s passa a l'estat: %s",acceptedControladors[x].name,getState(acceptedControladors[x].estat));
				msg(mis);

			}
		}else if(acceptedControladors[x].estat == SUBSCRIBED){
			if((timeinfo->tm_sec- acceptedControladors[x].lastHello) > 4)
			{


			}
		}

	}
	}
}

void subsFase(struct UdpPDU dades)
{

    if(comprovarPaquetUDP(dades))
    {
				tractarDadesSub(dades);

				generarSubAckPDU();
        enviarACK();

    }else{
        sendSubRej();
    }
}

void generarSubAckPDU(){
	int random;
	struct dadesFill info;
    socklen_t len;

	memset(&dadEnv,0,sizeof(struct UdpPDU));

	dadEnv.type = SUBS_ACK;
	strcpy(dadEnv.mac,server.mac);
	random = generateRandom();

	sprintf(acceptedControladors[controlador].number, "%d", random);
	strcpy(dadEnv.number,acceptedControladors[controlador].number);



	memset(&info,0,sizeof(struct dadesFill));
	info.type = RANDOM;
	strcpy(info.number,acceptedControladors[controlador].number);
	write(4,&info,sizeof(struct dadesFill));


  determinateNewUDP();

	if (getsockname(sockUDP,(struct sockaddr*)&addr_server,&len) == -1)
	    perror("getsockname");


	sprintf(dadEnv.dades,"%d",htons(addr_server.sin_port));

}

void determinateNewUDP()
{
    struct sockaddr_in addr_serverUDP;
    memset(&addr_serverUDP,0,sizeof (struct sockaddr_in));
    addr_serverUDP.sin_family=AF_INET;
    addr_serverUDP.sin_addr.s_addr=htonl(INADDR_ANY);
		addr_serverUDP.sin_port=0;

    if(bind(sockUDP,(struct sockaddr *)&addr_serverUDP,sizeof(struct sockaddr_in))<0)
	{
	    fprintf(stderr,"No puc fer el binding del socket!!! dasds\n");
	    exit(-2);
	}

}

void enviarACK(){
		int a;

		a = sendto(sockUDP,&dadEnv,sizeof(dadEnv),0,(struct sockaddr*)&addr_cli,sizeAddr);
		if(a > 0)
		{
			sprintf(mis,"Enviat:  bytes=%i, comanda=%s, mac=%s, rndm=%s, dades=%s",a,paquetType(dadEnv.type),dadEnv.mac,dadEnv.number,dadEnv.dades);
			dbg(mis);
		}else{
			sprintf(mis,"ERROR al enviar el paquet: %i",dadEnv.type);
			msg(mis);
		}
		acceptedControladors[controlador].estat = WAIT_INFO;

		sprintf(mis,"el controlador %s passa a l'estat %s",acceptedControladors[controlador].name,getState(acceptedControladors[controlador].estat));
		msg(mis);

}

void sendSubRej(){

}

char * getState(int stat){

	return state[stat - 0xa0].nom;

}

void infoFase(struct UdpPDU dades)
{

    if(comprovarPaquetUDP(dades)){

			tractarDadesInfo(dades);
			sendAckINFO();
    }else{


    }

}

void sendAckINFO()
{
	generarInfoAckPDU();
	enviarInfoAck();
}

void generarInfoAckPDU()
{
	memset(&paquetUDP,0,sizeof(struct UdpPDU));

	dadEnv.type = INFO_ACK;
	strcpy(paquetUDP.mac,server.mac);
	strcpy(paquetUDP.number,acceptedControladors[controlador].number);
	sprintf(paquetUDP.dades,"%i",server.tcpPort);

}

void enviarInfoAck()
{

	acceptedControladors[controlador].estat = SUBSCRIBED;

	sprintf(mis,"el controlador %s passa a l'estat %s",acceptedControladors[controlador].name,getState(acceptedControladors[controlador].estat));
	msg(mis);
}

void tractarDadesInfo(struct UdpPDU dades){

	int x,y, z;
	char temp[20];
	x = y = z = 0;
	while(dades.dades[x] != ',')
	{
		temp[x] = dades.dades[x];

		x++;

	}

	temp[x]='\0';
	x++;
	acceptedControladors[controlador].tcpPort =atoi(temp);
	while(dades.dades[x] != '\0')
	{
		z = 0;
		while(dades.dades[x] != ';' && dades.dades[x] != '\0')
		{
			temp[z] = dades.dades[x];
			x++;
			z++;
		}

		temp[z] = '\0';
		x++;
		strcpy(acceptedControladors[controlador].actuadors[y],temp);

		y++;

	}
	acceptedControladors[controlador].actNumero = y;

}


int determinateControler(char mac[])
{
		int x;
		for(x=0;x< nombreControladors; x++)
		{
			if (strcmp(acceptedControladors[x].mac,mac) == 0){
				return x;
			}
		}
		return 0; /* mai arribarem aqui, ja que venim de la funci paquetAcceptat que verificara que existeix */
}


int generateRandom(){

	int random;

	random = rand() % 99999999;
	while(random < 10000000)
	{
		random = random * 10;
	}

	return random;
}

int comprovarPaquetUDP(struct UdpPDU paquet)
{

		if(strcmp(acceptedControladors[controlador].mac,paquet.mac) == 0)
		{
			if(strcmp(acceptedControladors[controlador].number,paquet.number) == 0)
			{
				return 1;
			}
		}
	return 0;
}




void tractarDadesSub(struct UdpPDU paquet)
{
	int x,y;
	char candidateName[20];
	x = y = 0;


	while(paquet.dades[x] != ',')
	{
		candidateName[x] = paquet.dades[x];
		x++;
	}
	candidateName[x] = '\0';
	x++;
	if(strcmp(candidateName,acceptedControladors[controlador].name) == 0)
	{
		while(paquet.dades[x] != '\0')
		{
			acceptedControladors[controlador].situacio[y] = paquet.dades[x];
			x++;
			y++;
		}
	}
	acceptedControladors[controlador].situacio[y] = '\0';
}

void configureServer()
{
	char * line = NULL;
	size_t len = 0;
	ssize_t read = 0 ;
	int x = 0;
	char data[4][20];

    configuration_file = fopen(documentCFG, "r");
	while ((read = getline(&line, &len, configuration_file)) > 0)
	{
		situateStartData(line);
		strcpy(data[x],line);
		x++;
	}
	strcpy(server.name,data[0]);
	strcpy(server.mac,data[1]);
	server.udpPort = atoi(data[2]);
	server.tcpPort= atoi(data[3]);

	fclose(configuration_file);
}

void situateStartData(char *data)
{
	char temp[20];
	int x, y;

	x = 0;

	while(data[x] != '=')
	{
		x++;
	} /*sortim que estarem damunt del = */
	x += 2; /*avancem 2 posicions, que ens situaran al comencament de les dades*/
	y = 0;
	while(data[x] != '\n')
	{
		temp[y] = data[x];
		x++;
		y++;
	}
	temp[y] = '\0';
	strcpy(data,temp);



}


void configureControladors()
{
    FILE *controladors_file;
    char * line = NULL;
    char temp[20];
    char temp2[20];
		int x,y,j;
    size_t len =0;
		ssize_t read = 0 ;

		nombreControladors = 0;

    controladors_file = fopen(documentControladors, "r");
    if (controladors_file == NULL)
        exit(EXIT_FAILURE);

    x = 0;
    y = 0;
    j = 0;

    while ((read = getline(&line, &len, controladors_file)) > 0) {
				nombreControladors ++;
        x = 0;

        while(line[x] != ',')
        {
            temp[x] = line[x];
            x++;
        }

        temp[x] = '\0';
        strcpy(acceptedControladors[y].name,temp);
        x++;
        j = 0;

        while(line[x] != '\n')
        {
            temp2[j] = line[x];
            x++;
            j++;
        }

        temp2[j] = '\0';

        strcpy(acceptedControladors[y].mac,temp2);
				strcpy(acceptedControladors[y].number,"00000000");
				acceptedControladors[y].estat = DISCONNECTED;
				sprintf(acceptedControladors[y].situacio,"            ");
				sprintf(acceptedControladors[y].IP,"               ");
					sprintf(acceptedControladors[y].elements,"       ");
				acceptedControladors[y].noHello = 0;
        y++;



    }
		fclose(controladors_file);
}

char * paquetType(int type)
{
		if(type >= 0x10){
			return SecondFaseType[type - 0x10].nom;

		}else{
    	return SubsTypes[type].nom;
		}
}

void msg(char *msg){
		time_t rawtime;
    struct tm * timeinfo;

    time ( &rawtime );
    timeinfo = localtime ( &rawtime );

    printf("%d:%d:%d: MSG => %s \n",timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec,msg);
}


void dbg(char *msg)
{
    if(debug == TRUE)
    {
        time_t rawtime;
        struct tm * timeinfo;

        time ( &rawtime );
        timeinfo = localtime ( &rawtime );

        printf("%d:%d:%d: DEBUG => %s \n",timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec,msg);
    }
}
