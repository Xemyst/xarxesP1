#!/usr/bin/env python2.7
import socket
import sys, signal
import struct, time, optparse, select, os

t = 1
u = 2
n = 7
o = 3
p = 3
q = 3
v = 2
r = 2

class SocketUDP(): #objecte SocketUDP per mantenir les comunicacios de subscripcio i manteniment de hellos
    global last
    def __init__(self):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    def reciv(self):
            return self.s.recvfrom(103)
    def send(self,packet):

        self.s.sendto(packet, (Server,int(SrvUDP)))
    def sendto(self,envia, to, port):
        self.s.sendto(envia,(to, int(port)))
    def timeout(self,time):
        self.s.settimeout(time)
    def on_read(self):
        self.s.recvfrom(103)

    def fileno(self):
        return self.s.fileno()

class Input(): #objecte que correspondra al teclat
    def __init__(self):
        self.reader = sys.stdin
        self.s = ServerTCP()
    def fileno(self):
        return self.reader.fileno()
    def on_read(self):
        txt = self.reader.readline().split('\n')[0]
        txt = txt.split()
        if len(txt) > 0:
            if txt[0] == "stat":
                print "\n********************* DADES CONTROLADOR *********************"
                print "  MAC: " + MAC + ", Nom: "+ Name +  ", Situacio: " + Situation + "\n"
                print "  Estat: " + mystate() + "\n"
                print "  Dispos.    valor"
                print "  -------    -----"
                for element in elements:
                        print "  " + element[0] + "     " + element[1]
                print "\n************************************************************"
            elif txt[0] == "set":
                if len(txt) != 3:
                    msg("Error de sintaxi. (set <element> <valor> )")
                else:

                        for x in range (0,len(elements)):
                            if elements[x][0] == txt[1]:
                                elements[x][1] = txt[2]
                                return

                        msg("Element [" + txt[1] + "] no pertany al controlador")

            elif txt[0] == "send":

                if len(txt) != 2:
                    msg("Error de sintaxi. (set <element>)")
                else:

                        for x in range (0,len(elements)):
                            if elements[x][0] == txt[1]:
                                self.s.send(elements[x][0],elements[x][1])
                                return

                        msg("Element [" + txt[1] + "] no pertany al controlador")


            elif txt[0] == "quit":
                os.kill(newRef, signal.SIGKILL)
                debug("tancat el proces que enviava Hellos")
                sys.exit(0)
            else:
                msg("comanda incorrecta ( "+ txt[0] +" )")
class ServerTCP(): #socket per enviar dades al servidor
    def __init__(self):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    def fileno(self):
        return self.s.fileno()
    def send(self,element,data):
        self.s.connect(("",int(TCPServer)))
        data = struct.pack("B13s9s8s7s80s",SEND_DATA,MAC,idServer,element,data,"")
        self.s.send(data)
        self.s.close()
class SocketTCP(): #socket per rebre del servidor
    def __init__(self):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.bind(("",int(LocalTCP)))
        msg( "obert port " + LocalTCP + " per rebre dades del servidor")
        self.s.listen(1)
    def fileno(self):
        return self.s.fileno()
    def on_read(self):

        newsocket, address = self.s.accept()
        print "falta comprovar error de paquet (rebre de server)"
        a = newsocket.recv(103)
        a = unpack(a)[3].split("\x00")[:2]
        for x in range (0,len(elements)):
            if elements[x][0] == a[0]:
                elements[x][1] = a[1]
        debug("a")
        newsocket.close()


types = {
0x00: "SUBS_REQ",
0x01: "SUBS_ACK",
0x02: "SUBS_REJ",
0x03: "SUBS_INFO",
0x04: "INFO_ACK",
0x05: "SUBS_NACK"}

states = {0xa0:"DISCONNECTED",0xa1:"NOT_SUBSCRIBED",0xa2:"WAIT_ACK_SUBS", 0xa3:"WAIT_INFO",0xa4:"WAIT_ACK_INFO",0xa5:"SUBSCRIBED",0xa6:"SEND_HELLO"}
pause = t
packet_count = 0


SUBS_REQ = 0x00
SUBS_ACK = 0x01
SUBS_REJ = 0x02
SUBS_INFO = 0x03
INFO_ACK = 0x04
SUBS_NACK = 0x05

DISCONNECTED = 0xa0
NOT_SUBSCRIBED = 0xa1
WAIT_ACK_SUBS = 0xa2
WAIT_INFO = 0xa3
WAIT_ACK_INFO = 0xa4
SUBSCRIBED = 0xa5
SEND_HELLO = 0xa6

SEND_DATA = 0x20
SET_DATA = 0x21
GET_DATA = 0x22
DATA_ACK = 0x23
DATA_NACK = 0x24
DATA_REJ = 0x25

state = NOT_SUBSCRIBED

HELLO = 0x10
HELLO_REJ = 0x11
def main():

    init()
    setUP()
    subsFase()
    firstHello()
    openTCPports()
    serviceLoop()

def openTCPports():
    global sockTCP
    sockTCP = SocketTCP()

def firstHello(): #funcio destinada a enviar el primer hello
    global state
    paquet = pack(HELLO)
    i = 0
    state = SEND_HELLO
    msg("Controlador " + Name + " passa a l'estat " + mystate())

    sockUDP.send(paquet)
def serviceLoop():
    global newRef, state, sockTCP
    NonRecived = 0
    i = Input()

    newRef = os.fork()
    if newRef == 0:
        sendHello()
    else:
        last = 0
        while True:
                readers, _, _,=select.select([i,sockUDP,sockTCP], [] , [],2)
                for reader in readers:
                    if reader == sockUDP: #
                        reader.on_read() #cridem a la funcio  de lectura de cada objecte, ell sencarregara de fer el tractament
                        last = time.strftime("%S")
                    else:
                        reader.on_read()
                if abs((int(time.strftime("%S")) - int(last))) >= 2:
                    NonRecived += 1
                if NonRecived == 3:
                    msg("3 paquets no rebuts... s'inicia un nou proces de subscripcio")
                    state = NOT_SUBSCRIBED
                    os.kill(newRef,signal.SIGKILL)
                    break;
        subsFase()
        print mystate()

def sendHello(): #aquesta funcio sera la de manteniment de hellos, aqui arribarem amb el fill solament
    global sockUDP

    paquet = pack(HELLO)
    i = 0
    sockUDP.send(paquet)
    count = time.strftime("%S")
    while 1:
        if abs((int(time.strftime("%S")) - int(count))) >= 2:
            paquet = pack(HELLO)

            sockUDP.send(paquet)
            count = time.strftime("%S")

            i += 1



def subsAck():
    global infoFase

    packet_count = 0
    pause = t
    while infoFase == False and packet_count < 9: #bucle que enviara SUBS REQ fins rebre un ACK o acabar els intents

        try:
            sockUDP.timeout(pause) #determinem lestona que escoltarem
            envia = pack(SUBS_REQ) #empaquetem
            input_packet = unpack(envia)
            debug("Paquet Enviat: " + getType(input_packet[0]) + " MAC: " + str(input_packet[1]) + " idServer = " + str(input_packet[2]) + " Dades = " + str(input_packet[3]))
            sockUDP.send(envia) # enviem el paquet
            packet_count += 1
            state = WAIT_ACK_SUBS #ja que hem enviat canviem lestat

            (input_packet,data) = sockUDP.reciv() #103 tamany de la PDU
            input_packet = unpack(input_packet)
            debug("Paquet rebut: " + getType(input_packet[0]) + " MAC: " + str(input_packet[1]) + " idServer = " + str(input_packet[2]) + " Dades = " + str(input_packet[3]))

            if input_packet[0] == SUBS_ACK:
                infoFase = True
                saveSerData(input_packet)

        except:
            if packet_count > 3 and pause < (q*t):
                pause += 1
def infoAck():
    global state, sockUDP, TCPServer
    packet_count = 0
    pause = t
    while state != SUBSCRIBED and packet_count < 9: #bucle que enviara info SUBS fins rebre el ACK o acabar els intents
        try:
            sockUDP.timeout(pause) #determinem lestona que escoltarem

            paquet = pack(SUBS_INFO) #empaquetem

            sockUDP.sendto(paquet,Server,portSubs) # enviem el paquet
            debug("Enviat: " + "Bytes: "+ str(len(paquet))+ ", comanda: " + getType(SUBS_INFO) + " mac= "+ MAC +" rndom= " + str(idServer) )
            packet_count += 1

            state = WAIT_ACK_INFO #ja que hem enviat canviem lestat
            input_packet = sockUDP.reciv()[0] #103 tamany de la PDU
            input_packet = unpack(input_packet)
            debug("Paquet rebut: " + getType(input_packet[0]) + " MAC: " + str(input_packet[1]) + " idServer = " + str(input_packet[2]) + " Dades = " + str(input_packet[3]))

            TCPServer = input_packet[3][:5]
            if input_packet[0] == INFO_ACK:
                state = SUBSCRIBED
                msg("Controlador " + Name + " passa a l'estat " + mystate())
        except:
            if packet_count < 3 and packet_count < (q*t):
                pause += 1

def getType(type):
    return types.get(type)
def mystate():
    return states.get(state)
def subsFase():
    global infoFase, state, tries
    state = NOT_SUBSCRIBED
    msg("Controlador " + Name + " passa a l'estat " + mystate() + "intent de subscripcio" + str(tries))
    while state != SUBSCRIBED and tries != 3:
        infoFase = False
        packet_count = 0
        idServer = "00000000"

        subsAck()
        if infoFase == True:
            infoAck()
        tries += 1
        if state != SUBSCRIBED:
            time.sleep(u)
            msg("Controlador: " + Name + ", Intent de Subscripcio n " + str(tries) + " finalitzat..." )
    if state != SUBSCRIBED:
        msg("superat el nombre de processos de subscripcio( 3 )")
        sys.exit()

def setUP():
    global Name, elements, Situation, Elements, MAC, LocalTCP, Server, SrvUDP, archivo, idServer
    archivo = open(options.filename, "r")
    Name = readDoc()
    Situation = readDoc()

    Elements = readDoc()
    tmp = Elements.split(";")

    elements = []
    i = 0
    for x in range (0,len(tmp)):

        elements.append([])
        elements[i].append(tmp[x])
        elements[i].append("NONE")
        i += 1

    MAC = readDoc()
    LocalTCP = readDoc()
    Server = readDoc()
    SrvUDP = readDoc()
    idServer = '00000000'
def saveSerData(data):
    global idServer, serverMAC, portSubs
    idServer = data[2]
    serverMAC = data[1]
    portSubs = data[3][:5]
def readDoc():
    contenido = archivo.readline() #com que trenquem per espais sera "nom del camp"[0] "="[1] "contingut"[2]
    contenido = contenido.split()
    return contenido[2]

def init():
    global sockUDP

    sockUDP = SocketUDP()

def pack(type):
    if type == SUBS_INFO:
        return struct.pack("B13s9s80s",type,MAC,idServer,LocalTCP+","+Elements)
    else:
        return struct.pack("B13s9s80s",type, MAC, idServer ,Name+"," + Situation)
def unpack(pack):
    input_packet = struct.unpack("B13s9s80s",pack)
    return input_packet
def debug(msg):
    if options.debug == True:
        print time.strftime("%H:%M:%S") + ": DEBUG => " + msg
def msg(msg):
        print time.strftime("%H:%M:%S") + ": MSG. => " + msg


def correct(packet):
    return True
if __name__ == '__main__':

    global tries

    parser = optparse.OptionParser()
    parser.add_option ('-c', '--file', action='store', type='string', default='./client.cfg', help='PATH to config file', dest="filename")
    parser.add_option ('-d', '--debug', action='store_true', default=False, help='debug', dest="debug")
    (options, args) = parser.parse_args()

    tries = 0
    main()
